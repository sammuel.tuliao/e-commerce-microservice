package com.productmanagement.productmanagement.controller;

import com.productmanagement.productmanagement.dto.UsersDto;
import com.productmanagement.productmanagement.entity.Products;
import com.productmanagement.productmanagement.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@RestController
@RequestMapping("/api/ecommerce")
public class ProductsController {
    @Autowired
    private ProductsService productsService;
    @PostMapping("/add-products")
    public String addProduct (Products productsDetail){
        return productsService.addProducts(productsDetail);
    }
    @GetMapping("/product-list")
    public Iterable<Products> getAllProduct (){
        return productsService.listProducts();
    }

    @GetMapping("/product-info")
    public Products getById (@RequestParam int product_id){
            return productsService.findProductById(product_id);
    }
    @DeleteMapping("/delete-product")
    public String deleteById (@RequestParam int product_id,@RequestParam int user_id){
        return productsService.deleteProduct(product_id,user_id);
    }

    @PatchMapping("/update-product")
    public String updateProduct (@RequestParam int product_id,@RequestParam int user_id,@RequestBody Products productsDetails){
        return productsService.updateProduct(product_id,user_id,productsDetails);
    }
}
