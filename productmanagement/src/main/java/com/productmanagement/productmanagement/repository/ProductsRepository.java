package com.productmanagement.productmanagement.repository;

import com.productmanagement.productmanagement.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ProductsRepository extends JpaRepository<Products, Long> {

}
