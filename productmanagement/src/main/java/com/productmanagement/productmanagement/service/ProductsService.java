package com.productmanagement.productmanagement.service;

import com.productmanagement.productmanagement.dto.UsersDto;
import com.productmanagement.productmanagement.entity.Products;
import com.productmanagement.productmanagement.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class ProductsService {

    @Autowired
    private ProductsRepository productsRepository;
    private RestTemplate restTemplate = new RestTemplate();
    public String addProducts(Products productsDetails){
        if(Objects.equals(getUserRole(productsDetails.getUserId()), "SELLER")){
            productsRepository.save(productsDetails);
            return "Product added!";
        }
        else{
            return "You don't have Access!";
        }
    }
    public String updateProduct(int product_id,int user_id,Products productsDetails){
        try {
            if(getUserIdApi(user_id) == null){return "User does not Exist!";}
            if(Objects.equals(getUserRole(user_id), "BUYER")){return "You don't have Access!";}
            if(!isProductExists(product_id)){return "Product does not Exist!";}
            if(productsDetails.getUserId() != user_id){return "You don't own this Product!";}
            if(isProductExists(productsDetails.getProductId()) && Objects.equals(getUserRole(user_id), "SELLER") && productsDetails.getUserId() == user_id){
                Products updateProduct = productsRepository.findById((long)product_id).orElse(null);
                updateProduct.setProductId(productsDetails.getProductId());
                updateProduct.setProductName(productsDetails.getProductName());
                updateProduct.setProductDescription(productsDetails.getProductDescription());
                updateProduct.setProductQuantity(productsDetails.getProductQuantity());
                updateProduct.setProductPrice(productsDetails.getProductPrice());
                updateProduct.setProductImage(productsDetails.getProductImage());
                updateProduct.setUserId(productsDetails.getUserId());
                productsRepository.save(updateProduct);
                return "Product Updated";
            }
            else {return "Product does not Exist!";}
        }catch (Exception e){
            return "Check your Input!";
        }
    }
    public String deleteProduct(int product_id,int user_id){
        try {
            Products checkProducts = findProductById(product_id);
            if(getUserIdApi(user_id) == null){return "User does not Exist!";}
            if(Objects.equals(getUserRole(user_id), "BUYER")){return "You don't have Access!";}
            if(!isProductExists(product_id)){return "Product does not Exist!";}
            if(checkProducts.getUserId() != user_id){return "You don't own this Product!";}
            if(isProductExists(product_id)  && Objects.equals(getUserRole(user_id), "SELLER") && checkProducts.getUserId() == user_id){
                productsRepository.delete(findProductById(product_id));
                return "Product Deleted";
            }
            else{return "Product does not Exist!";}
        }catch (Exception e){
            return "Check your Input!";
        }
    }
    public List<Products> listProducts(){
        return productsRepository.findAll();
    }
    public Products findProductById(int product_id){
            return productsRepository.findById((long)product_id).orElse(null);
    }
    private boolean isProductExists(int product_id){
        if(findProductById(product_id)!= null){
            return true;
        }
        else {return false;}
    }
    private String getUserRole(int user_id){
        UsersDto resultApi = restTemplate.getForObject("http://localhost:8080/api/ecommerce/user-info?user_id="+user_id,UsersDto.class);
        if(resultApi==null){
            return null;
        }
        return resultApi.getRole();
    }

    private String getUserIdApi(int user_id){
        UsersDto resultApi = restTemplate.getForObject("http://localhost:8080/api/ecommerce/user-info?user_id="+user_id,UsersDto.class);
        if(resultApi==null){
            return null;
        }
        return String.valueOf(resultApi.getUserId());
    }



}
