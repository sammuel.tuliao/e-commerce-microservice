# E-commerce-microservice



1.What are the functionalities involved, and the microservices you've created

-Module that handles the creation,update,delete,search all users, and search user by id.

1.usermanagement
- addUser - Allows you to create a new User with unique email.
- getAllUser - Lists all registered user.
- getById - allows you to search for a specific user using user_id.
- deleteById - allows you to delete a specific user using user_id.
- updateUser - allows you to update the contents of the existing user.

-Module that handles the creation,update,delete,list all products, and seach a product by its id.

2.productmanagement 
-addProduct - Allows you to create a new product if you are registered as a SELLER.
-getAllProduct - Allows you to list all of registered products.
-getById - Allows you to search for a specific product by using product_id.
-deleteById - Allows you to delete a product. 
-updateProduct  - Allows you to update the contents of the existing user.

-Module that handles the adding item, removing item, and listing the item in cart.

3.cartmanagement
- showCartList list the items in your cart.
- addToCart  allows you to add a product in your cart if you are registered as a BUYER.
- removeFromCart allows you to remove your added product inside your cart.

2.Why do you think that the functionality needs to be isolated from another service.
 Because if the project is too big, it will be hard to fix bugs because you have to shut down the whole application/service 
 just to fix that bug. And I also think that isolating functionalities is good as a developer because it is reusable.
 Also different technologies can be used for each services. Some programming languages favors some services.
 and last is its scalability you can change one service and have little to no effect on other services.
