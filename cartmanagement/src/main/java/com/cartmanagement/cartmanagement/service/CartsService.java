package com.cartmanagement.cartmanagement.service;
import com.cartmanagement.cartmanagement.dto.ProductsDto;
import com.cartmanagement.cartmanagement.dto.UsersDto;
import com.cartmanagement.cartmanagement.entity.Carts;
import com.cartmanagement.cartmanagement.repository.CartsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
@Service
public class CartsService {

    @Autowired
    private CartsRepository cartsRepository;
    RestTemplate restTemplate = new RestTemplate();

    public String addItemToCart(int user_id, int product_id){
        if(getProductIdApi(product_id) == null){return "Product does not Exists!";}
        if(getUserIdApi(user_id) ==null){return"User does not Exists!";}
        if(Objects.equals(getUserRole(user_id), "SELLER")) {return "You don't have access";}
            Carts newItem = new Carts();
            newItem.setProductId(product_id);
            newItem.setUserId(user_id);
            cartsRepository.save(newItem);
            return "Product added to Cart!";

    }
    public String removeItemToCart(int user_id,Carts cartsDetails) {
        if(getProductIdApi(cartsDetails.getProductId()) == null ||
                !checkItem(user_id,cartsDetails.getProductId())){return "item does not exist in cart";}
        if(getUserIdApi(cartsDetails.getUserId()) ==null){return"User does not Exists!";}
        if(Objects.equals(getUserRole(user_id), "SELLER")) {return "You don't have access";}
        Carts removeItem = cartsRepository.findById((long) cartsDetails.getCartId()).orElse(null);
        assert removeItem != null;
        removeItem.setCartId(cartsDetails.getCartId());
        removeItem.setProductId(cartsDetails.getProductId());
        removeItem.setUserId(cartsDetails.getUserId());
        cartsRepository.delete(removeItem);
        return "Product removed from cart";
    }
    public String listItemInCart(int user_id){
            if(getUserIdApi(user_id)==null){return"User does not Exists";}
            if(Objects.equals(getUserRole(user_id), "SELLER")){return "You don't have Permission"; }
            if(findByUserId(user_id)){
                StringBuilder output = new StringBuilder();
                for(int i = 0; i < getProductDetailsApi().size(); i++){
                    for(int j = 0; j < cartsRepository.findByUserIdAndProductId(user_id,getProductDetailsApi().get(i).getProductId()).size(); j++){
                    if(checkItem(user_id,getProductDetailsApi().get(i).getProductId())){
                output.append(getProductDetailsApi().get(i).getProductName()).append(" ");
                output.append(getProductDetailsApi().get(i).getProductDescription()).append(" ");
                output.append(getProductDetailsApi().get(i).getProductPrice()).append("\n");}
                    }
                }
                return output.toString();
            }else{
                return "You don't have items in cart";}
    }
    public boolean findByUserId(int user_id){
        if(!cartsRepository.findByUserId(user_id).isEmpty()){return true;}
        else{return false;}
    }
    public boolean checkItem(int user_id,int product){
        if(!cartsRepository.findByUserIdAndProductId(user_id,product).isEmpty()){return true;}
        else{return false;}
    }
    private String getUserIdApi(int user_id){
        UsersDto resultApi = restTemplate.getForObject("http://localhost:8080/api/ecommerce/user-info?user_id="+user_id,UsersDto.class);
        if(resultApi==null){
            return null;
        }
        return String.valueOf(resultApi.getUserId());
    }
    private String getUserRole(int user_id){
        UsersDto resultApi = restTemplate.getForObject("http://localhost:8080/api/ecommerce/user-info?user_id="+user_id,UsersDto.class);
        if(resultApi==null){
            return null;
        }
        return resultApi.getRole();
    }
    private String getProductIdApi(int product_id){
        ProductsDto resultApi = restTemplate.getForObject("http://localhost:8081/api/ecommerce/product-info?product_id="+product_id,ProductsDto.class);
        if(resultApi==null){
            return null;
        }
        return String.valueOf(resultApi.getProductId());
    }
    private List<ProductsDto> getProductDetailsApi(){
        ProductsDto[] resultApi = restTemplate.getForObject("http://localhost:8081/api/ecommerce/product-list",ProductsDto[].class);
        if(resultApi==null){
            return null;
        }
        return Arrays.asList(resultApi);
    }
}
