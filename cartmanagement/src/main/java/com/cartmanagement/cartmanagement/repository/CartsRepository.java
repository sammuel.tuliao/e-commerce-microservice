package com.cartmanagement.cartmanagement.repository;
import com.cartmanagement.cartmanagement.entity.Carts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CartsRepository extends JpaRepository<Carts, Long> {
    List<Carts> findByUserId(int user_id);
    List<Carts> findByUserIdAndProductId(int userId,int productId);



}