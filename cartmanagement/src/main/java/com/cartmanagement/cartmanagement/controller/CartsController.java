package com.cartmanagement.cartmanagement.controller;

import com.cartmanagement.cartmanagement.entity.Carts;
import com.cartmanagement.cartmanagement.service.CartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ecommerce")
public class CartsController {
    @Autowired
    CartsService cartsService;
    @GetMapping("/cart-list")
    public  String showCartList(@RequestParam int user_id){
        return String.valueOf(cartsService.listItemInCart(user_id));
    }

    @PostMapping("/add-item")
    public String addToCart(@RequestParam int user_id, @RequestParam int product_id){
        return cartsService.addItemToCart(user_id,product_id);
    }


    @DeleteMapping("/remove-item")
    public String removeFromCart(@RequestParam int user_id,@RequestBody Carts cartsDetails){
        return cartsService.removeItemToCart(user_id,cartsDetails);
    }
}
