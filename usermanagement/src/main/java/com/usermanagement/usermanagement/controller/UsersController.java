package com.usermanagement.usermanagement.controller;

import com.usermanagement.usermanagement.entity.Users;
import com.usermanagement.usermanagement.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/ecommerce")
public class UsersController {
    @Autowired
    private UsersService usersService;

    @PostMapping("/add-user")
    public String addStudents (Users users){
        return usersService.addUser(users);
    }
    @GetMapping("/user-list")
    public Iterable<Users> getAllStudent (){
        return usersService.listUsers();
    }
    @GetMapping("/user-info")
    public Users getById (@RequestParam int user_id){
        return usersService.findUserById(user_id);
    }

    @DeleteMapping("/delete-user")
    public String deleteById (@RequestParam int user_id){
        return usersService.deleteUser(user_id);
    }

    @PatchMapping("/update-user")
    public String updateStudent (@RequestParam int user_id,@RequestBody Users usersDetails){
        return usersService.updateUser(user_id,usersDetails);
    }




}
