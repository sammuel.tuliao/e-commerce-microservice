package com.usermanagement.usermanagement.service;

import com.usermanagement.usermanagement.entity.Users;
import com.usermanagement.usermanagement.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;
    public String addUser(Users usersDetails){
        try {
            if(usersRepository.findByEmail(usersDetails.getEmail()) == null){
                usersRepository.save(usersDetails);
                return "User added!";
            }
            else{ return "User Already Exists!";}
        }catch (Exception e){
            return "Check your Input!";
        }
    }
    public String updateUser(int user_id,Users usersDetails){
        try{
            if(isExists(usersDetails.getUserId())){
                Users updateUser = usersRepository.findById((long)user_id).orElse(null);
                updateUser.setUserId(usersDetails.getUserId());
                updateUser.setFirstName(usersDetails.getFirstName());
                updateUser.setLastName(usersDetails.getLastName());
                updateUser.setEmail(usersDetails.getEmail());
                updateUser.setPassword(usersDetails.getPassword());
                updateUser.setRole(usersDetails.getRole());
                usersRepository.save(updateUser);
                return "User Updated";
            }
            else {return "User not Found";}
        }catch (Exception e){
            return "Check your Input!";
        }

    }
    public String deleteUser(int user_id){
        if(isExists(user_id)){
            usersRepository.delete(findUserById(user_id));
            return "User Deleted";
        }
        else{ return "User does not Exist!";}
    }
    public List<Users> listUsers(){
        return usersRepository.findAll();
    }
    public Users findUserById(int user_id){
        return usersRepository.findById((long)user_id).orElse(null);
    }


    private boolean isExists(int user_id){

        if(findUserById(user_id)!= null){
            return true;
        }
        else {return false;}
    }
}
